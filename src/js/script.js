$('a[href="#"]').on('click', function (e) {
  e.preventDefault();
});

//Header Scticky
$(window).scroll(function() {
  if ($(this).scrollTop() > 0) {
    $('header').addClass("sticky");
  }
  else {
    $('header').removeClass("sticky");
  }
});

// Smooth scrolling
$('a[href^="#"]').bind('click.smoothscroll',function (e) {
  e.preventDefault();

  var target = this.hash,
        $target = $(target);

  $('html, body').stop().animate({'scrollTop': $target.offset().top - 83},
        {
          // Свойство анимации
          duration: 500, // Время анимации
          // Шаги анимации (http://api.jquery.com/animate/)
          step: function (progress) {
            // Когда скролл равен или больше высоты окна, то снимается checked с input'а
            if (progress >= 10) $('#hamburger').prop('checked', false);
          }
        }, 'swing');

});

// Min height review title
let maxHeight = 0;
if ($(window).width() >= 1201) {
  $('.reviews__item h3.reviews__item--title').each(function (ind, el) {
    let currentHeight = $(el).height();
    if (maxHeight < currentHeight) {
      maxHeight = currentHeight;
    }
  });
  $('.reviews__item--title').css('min-height', maxHeight - 44);
}
